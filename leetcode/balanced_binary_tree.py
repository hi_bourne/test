# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from tree_node import TreeNode

class Solution(object):
    
    def isBalanced(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        ret = self.isBalance(root)
        if ret != -1:
            return True
        else:
            return False

    def isBalance(self, root):
        if not root:
            return 0
        left_hight = self.isBalance(root.left)
        if left_hight == -1:
            return -1
        right_hight = self.isBalance(root.right)
        if right_hight == -1:
            return -1
        minus = left_hight - right_hight;
        if minus > 1  or minus < -1:
            return -1
        if minus < 0:
            return right_hight + 1
        else:
            return left_hight + 1

if __name__ == '__main__':

    node = TreeNode(1)
    node.right = TreeNode(2)
    node.right.right = TreeNode(3)
    solu = Solution()

    print solu.isBalanced(node)
