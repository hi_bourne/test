# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from tree_node import TreeNode

class Solution(object):
    def invertTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if not root:
            return None
        tmp_node = root.left
        root.left = root.right
        root.right = tmp_node
        self.invertTree(root.left)
        self.invertTree(root.right)
        return root

    def printPath(self, root):
        if not root:
            return
        print root.val,
        if root.left:
            self.printPath(root.left)
        if root.right:
            self.printPath(root.right)

if __name__ == '__main__':

    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    so = Solution()
    so.printPath(root)
    print
    ret = so.invertTree(root)
    so.printPath(ret)
