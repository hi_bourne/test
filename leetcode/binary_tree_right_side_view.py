# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from tree_node import TreeNode

class Solution(object):
    def rightSideView(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        first_list = []
        second_list = []
        ret_list = []
        if not root:
            return ret_list
        first_list.append(root)
        while len(first_list) > 0:
            ret_list.append(first_list[-1].val)
            for item in first_list:
                if item.left:
                    second_list.append(item.left)
                if item.right:
                    second_list.append(item.right)
            first_list = second_list
            second_list = []
        return ret_list

if __name__ == '__main__':
    root = TreeNode(1)
    root.left = TreeNode(3)
    root.right = TreeNode(2)

    so = Solution()
    ret = so.rightSideView(root)
    for item in ret:
        print item,
