# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from tree_node import TreeNode

class Solution(object):
    def invertTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if root:
            root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)
            return root

    def printPath(self, root):
        if not root:
            return
        print root.val,
        if root.left:
            self.printPath(root.left)
        if root.right:
            self.printPath(root.right)

if __name__ == '__main__':

    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    so = Solution()
    so.printPath(root)
    print
    ret = so.invertTree(root)
    so.printPath(ret)
