# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from tree_node import TreeNode

class Solution(object):
    
    def isBalanced(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        return self.isBalance(root) != -1

    def isBalance(self, root):
        if not root:
            return 0
        left_hight = self.isBalance(root.left)
        if left_hight == -1:
            return -1
        right_hight = self.isBalance(root.right)
        if right_hight == -1:
            return -1
        if abs(left_hight - right_hight) > 1:
            return -1
        return max(left_hight, right_hight) + 1

if __name__ == '__main__':

    node = TreeNode(1)
    node.right = TreeNode(2)
    node.right.right = TreeNode(3)
    solu = Solution()

    print solu.isBalanced(node)
